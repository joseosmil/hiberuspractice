﻿namespace GNB.WebApp.Models
{
    public class Transaction
    {
        public string SKU { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
    }
}
