﻿using GNB.Core.Interfaces.Services;
using GNB.WebApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace GNB.WebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ITransactionService _transactionService;
        private readonly ILogger<HomeController> _logger;

        public HomeController(ITransactionService transactionService, ILogger<HomeController> logger)
        {
            _transactionService = transactionService;
            _logger = logger;
        }

        public async Task<IActionResult> Index(string sku)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(sku)) return View();

                var transactions = await _transactionService.GetAllFromApi(sku);
                var transactionModel = transactions.Select(t => new Transaction {
                     SKU = t.SKU,
                     Currency = t.Currency,
                     Amount = t.Amount                
                });

                return View(transactionModel);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return Error();
            }
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
