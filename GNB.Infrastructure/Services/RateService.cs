﻿using GNB.Core.Interfaces.Repositories;
using GNB.Core.Interfaces.Services;
using GNB.Core.Model;
using GNB.Core.Model.DTO;
using GNB.Core.Utils;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace GNB.Infrastructure.Services
{
    public class RateService : IRateService
    {
        private readonly IRateRepository _rate;
        private readonly IConfiguration _configuration;
        private readonly ILogger<RateService> _logger;

        protected Mapper mapper;

        public RateService(IRateRepository rate,
            IConfiguration configuration, ILogger<RateService> logger)
        {
            _rate = rate;
            _configuration = configuration;
            _logger = logger;
            mapper = new Mapper();
        }

        public async Task<IEnumerable<RateDto>> GetAll()
        {
            try
            {
                var client = new RestClient(_configuration.GetSection("ApiServiceProvider:BaseURL").Value);
                var request = new RestRequest(_configuration.GetSection("ApiServiceProvider:RateResource").Value, DataFormat.Json);

                IRestResponse response = await client.ExecuteAsync(request, Method.GET);

                if (response.StatusCode != HttpStatusCode.OK || string.IsNullOrWhiteSpace(response.Content))
                {
                    _logger.LogError(response.ErrorMessage);
                    var rates = await _rate.GetAllAsync();
                    return mapper.Map(rates);
                }

                await _rate.DeleteAllAsync();

                var rateList = JsonConvert.DeserializeObject<IEnumerable<Rate>>(response.Content).ToList();

                await _rate.AddAsync(rateList);
                
                await _rate.SaveAsync();

                return mapper.Map(rateList);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw;
            }
        }

        public async Task<IEnumerable<RateDto>> GetAllFromApi()
        {
            try
            {
                var client = new RestClient(_configuration.GetSection("ApiServiceProvider:BaseURL").Value);
                var request = new RestRequest(_configuration.GetSection("ApiServiceProvider:RateResource").Value, DataFormat.Json);

                IRestResponse response = await client.ExecuteAsync(request, Method.GET);

                if (response.StatusCode != HttpStatusCode.OK || string.IsNullOrWhiteSpace(response.Content))
                {
                    _logger.LogError(response.ErrorMessage);
                    return null;
                }

                var rateList = JsonConvert.DeserializeObject<IEnumerable<Rate>>(response.Content).ToList();

                return mapper.Map(rateList);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw;
            }
        }
    }
}