﻿using GNB.Core.Interfaces.Repositories;
using GNB.Core.Interfaces.Services;
using GNB.Core.Model;
using GNB.Core.Model.DTO;
using GNB.Core.Utils;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace GNB.Infrastructure.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly ITransactionRepository _transaction;
        private readonly IRateService _rateService;
        private readonly IConfiguration _configuration;
        private readonly ILogger<TransactionService> _logger;

        protected Mapper mapper;

        public TransactionService(ITransactionRepository transaction, IRateService rateService,
            IConfiguration configuration, ILogger<TransactionService> logger)
        {
            _transaction = transaction;
            _rateService = rateService;
            _configuration = configuration;
            _logger = logger;
            mapper = new Mapper();
        }

        public async Task<IEnumerable<TransactionDto>> GetAll()
        {
            try
            {
                var client = new RestClient(_configuration.GetSection("ApiServiceProvider:BaseURL").Value);
                var request = new RestRequest(_configuration.GetSection("ApiServiceProvider:TransactionResource").Value, DataFormat.Json);

                IRestResponse response = await client.ExecuteAsync(request, Method.GET);

                if (response.StatusCode != HttpStatusCode.OK || string.IsNullOrWhiteSpace(response.Content))
                {
                    _logger.LogError(response.ErrorMessage);

                    return mapper.Map(await _transaction.GetAllAsync());
                }

                await _transaction.DeleteAllAsync();

                var transactions = JsonConvert.DeserializeObject<IEnumerable<TransactionDto>>(response.Content).ToList();

                await _transaction.AddAsync(mapper.Map(transactions));

                await _transaction.SaveAsync();

                return transactions;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw;
            }
        }

        public async Task<IEnumerable<TransactionDto>> GetAllFromApi(string sku)
        {
            try
            {
                var client = new RestClient(_configuration.GetSection("ApiServiceProvider:BaseURL").Value);
                var request = new RestRequest(_configuration.GetSection("ApiServiceProvider:TransactionResource").Value, DataFormat.Json);

                IRestResponse response = await client.ExecuteAsync(request, Method.GET);

                if (response.StatusCode != HttpStatusCode.OK || string.IsNullOrWhiteSpace(response.Content))
                {
                    _logger.LogError(response.ErrorMessage);
                    return null;
                }

                var transactions = JsonConvert.DeserializeObject<IEnumerable<Transaction>>(response.Content).Where(t => t.SKU == sku).ToList();                
                var rates = await _rateService.GetAllFromApi();

                transactions.ForEach(t => t.ConvertToCurrency(mapper.Map(rates), _configuration.GetSection("Currency").Value));

                return mapper.Map(transactions);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw;
            }
        }
    }
}