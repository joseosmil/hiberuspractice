﻿using GNB.Core.Interfaces.Repositories;
using GNB.Core.Model;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GNB.Infrastructure.Repositories
{
    public class RateRepository : IRateRepository
    {
        private readonly GNBDBContext _gNBContext;

        public RateRepository(GNBDBContext gNBDBContext)
        {
            _gNBContext = gNBDBContext;
        }

        public async Task AddAsync(IEnumerable<Rate> Entities)
        {
            await _gNBContext.AddRangeAsync(Entities);
        }

        public async Task DeleteAllAsync()
        {
            _gNBContext.RemoveRange(_gNBContext.Rates);
        }

        public async Task SaveAsync()
        {
            await _gNBContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<Rate>> GetAllAsync()
        {
            return await _gNBContext.Rates.ToListAsync();
        }
    }
}
