﻿using GNB.Core.Interfaces.Repositories;
using GNB.Core.Model;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GNB.Infrastructure.Repositories
{
    public class TransactionRepository : ITransactionRepository
    {
        private readonly GNBDBContext _gNBContext;

        public TransactionRepository(GNBDBContext gNBDBContext)
        {
            _gNBContext = gNBDBContext;
        }

        public async Task AddAsync(IEnumerable<Transaction> Entities)
        {
            await _gNBContext.AddRangeAsync(Entities);
        }

        public async Task DeleteAllAsync()
        {
            _gNBContext.RemoveRange(_gNBContext.Rates);
        }
        public async Task SaveAsync()
        {
            await _gNBContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<Transaction>> GetAllAsync()
        {
            return await _gNBContext.Transactions.ToListAsync();
        }
    }
}
