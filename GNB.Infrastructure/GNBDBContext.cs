﻿using GNB.Core.Model;
using Microsoft.EntityFrameworkCore;

namespace GNB.Infrastructure
{
    public class GNBDBContext : DbContext
    {
        public GNBDBContext(DbContextOptions<GNBDBContext> options) : base(options)
        {
        }

        public DbSet<Rate> Rates { get; set; }
        public DbSet<Transaction> Transactions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Rate>(r =>
            {
                r.Property<int>("Id").ValueGeneratedOnAdd();
                r.HasKey("Id");
                r.Property(e => e.From).HasMaxLength(4).IsRequired();
                r.Property(e => e.To).HasMaxLength(4).IsRequired();
                r.Property(e => e.rate).HasColumnType("decimal(18,2)").IsRequired();
            });

            modelBuilder.Entity<Transaction>(t =>
            {
                t.Property(e => e.Id).ValueGeneratedOnAdd();
                t.HasKey(e => e.Id);
                t.Property(e => e.SKU).HasMaxLength(10).IsRequired();
                t.Property(e => e.Currency).HasMaxLength(4).IsRequired();
                t.Property(e => e.Amount).HasColumnType("decimal(18,2)").IsRequired();
            });

            base.OnModelCreating(modelBuilder);
        }
    }
}
