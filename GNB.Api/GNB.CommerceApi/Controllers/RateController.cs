﻿using GNB.Core.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Threading.Tasks;

namespace GNB.CommerceApi.Controllers
{
    [Route("api/[controller]")]  
    [ApiController]
    public class RateController : ControllerBase
    {
        private readonly IRateService _rateService;
        private readonly ILogger<RateController> _logger;

        public RateController(IRateService rateService, ILogger<RateController> logger)
        {
            _rateService = rateService;
            _logger = logger;
        }

        [HttpGet]
        [Produces("application/json")]
        public async Task<IActionResult> Get()
        {
            try
            {
                var rates = await _rateService.GetAll();

                return Ok(rates);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }        
        }
    }
}