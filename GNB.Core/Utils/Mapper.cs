﻿using GNB.Core.Model;
using GNB.Core.Model.DTO;
using System.Collections.Generic;
using System.Linq;

namespace GNB.Core.Utils
{
    public class Mapper
    {
        public Rate Map(RateDto rateDto)
        {
            return new Rate
            {
                From = rateDto.From,
                To = rateDto.To,
                rate = rateDto.rate
            };
        }

        public IEnumerable<Rate> Map(IEnumerable<RateDto> ratesDto)
        {
            return ratesDto.Select(r => new Rate{               
                From = r.From,
                To = r.To,
                rate = r.rate
            });
        }

        public IEnumerable<RateDto> Map(IEnumerable<Rate> rates)
        {
            return rates.Select(r => new RateDto
            {
                From = r.From,
                To = r.To,
                rate = r.rate
            });
        }

        public Transaction Map(TransactionDto transactionDto)
        {
            return new Transaction
            {
                SKU = transactionDto.SKU,
                Currency = transactionDto.Currency,
                Amount = transactionDto.Amount
            };
        }

        public IEnumerable<Transaction> Map(IEnumerable<TransactionDto> transactionDtoList)
        {
            return transactionDtoList.Select(t => new Transaction
            {
                SKU = t.SKU,
                Currency = t.Currency,
                Amount = t.Amount
            });
        }

        public IEnumerable<TransactionDto> Map(IEnumerable<Transaction> transactions)
        {
            return transactions.Select(t => new TransactionDto
            {
                SKU = t.SKU,
                Currency = t.Currency,
                Amount = t.Amount
            });
        }
    }
}
