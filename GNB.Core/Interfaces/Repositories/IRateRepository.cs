﻿using GNB.Core.Model;

namespace GNB.Core.Interfaces.Repositories
{
    public interface IRateRepository : IGenericRepository<Rate>
    {
    }
}
