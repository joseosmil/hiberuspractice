﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GNB.Core.Interfaces.Repositories
{
    public interface IGenericRepository<T> where T: class
    {
        Task AddAsync(IEnumerable<T> Entities);
        Task DeleteAllAsync();
        Task SaveAsync();
         
        Task<IEnumerable<T>> GetAllAsync();       
    }
}