﻿using GNB.Core.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GNB.Core.Interfaces.Services
{
    public interface ITransactionService
    {
        Task<IEnumerable<TransactionDto>> GetAll();
        Task<IEnumerable<TransactionDto>> GetAllFromApi(string sku);
    }
}
