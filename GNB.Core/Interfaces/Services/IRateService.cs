﻿using GNB.Core.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GNB.Core.Interfaces.Services
{
    public interface IRateService
    {
        Task<IEnumerable<RateDto>> GetAll();
        Task<IEnumerable<RateDto>> GetAllFromApi();
    }
}
