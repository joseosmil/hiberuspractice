﻿namespace GNB.Core.Model
{
    public class Rate
    {
        public string From { get; set; }
        public string To { get; set; }
        public decimal rate { get; set; }
    }
}
