﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GNB.Core.Model
{
    public class Transaction
    {
        public Guid Id { get { return Guid.NewGuid(); } set { Id = value; } }
        public string SKU { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }

        public Transaction ConvertToCurrency(IEnumerable<Rate> rates, string currency)
        {
            decimal currencyAmount = GetCurrencyAmount(rates, currency);

            return new Transaction
            {
                SKU = SKU,
                Currency = currency,
                Amount = currencyAmount
            };
        }

        private decimal GetCurrencyAmount(IEnumerable<Rate> rates, string currency)
        {
            while (Currency != currency)
            {
                var rate = rates.Where(r => r.From == Currency && r.To == currency).FirstOrDefault();

                if (rate == null)
                {
                    var fromRates = new List<Rate>();
                    var currencies = rates.Where(r => r.To != Currency).ToList();

                    foreach (var item in currencies)
                    {
                        if (item.From != currency)
                        {
                            rate = currencies.Where(r => r.From == item.To && r.From != currency).FirstOrDefault();
                            if (rate != null && !fromRates.Contains(rate)) fromRates.Add(rate);
                        }
                    }

                    rate = currencies.Where(r => r.From == Currency && fromRates.Select(f => f.From).Contains(r.To)).FirstOrDefault();
                }

                Currency = rate.To;
                Amount = Math.Round(Amount * rate.rate, 2, MidpointRounding.ToEven);
            }

            return Amount;  
        }
    }
}