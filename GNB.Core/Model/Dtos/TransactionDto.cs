﻿namespace GNB.Core.Model.DTO
{
    public class TransactionDto
    {
        public string SKU { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
    }
}
