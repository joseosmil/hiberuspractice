﻿namespace GNB.Core.Model.DTO
{
    public class RateDto
    {
        public string From { get; set; }
        public string To { get; set; }
        public decimal rate { get; set; }
    }
}
