# GNB - Hiberus Practice#

GNB (Gloiath National Bank) Aplicación para consulta de ventas de los productos de comercialización de GNB. PO: Barney Stinson

Practica de solicitud de empleo.

### Tecnologías  ###

* Target Framework: .Net Core 3.1
* ORM: Entity Framework Core 3.1
* Data Base Engine: SQL Server 
* Api Data Interchange: Api Rest (json)
* UI: Asp.Net Core

### Arquitectura de la Solución ###

La solución separa las distintas responsabilidades en capas utilizando un diseño basado en el dominio:
* Capa de servicio: GNB.Api
* Capa Acceso a Datos: GBN.Infrastructure
* Capa de Definición del Dominio: GNB.Core
* Capa de Presentación: GNB.UI
* Capa de Pruebas: GBN.Test

### Requisitos ###

* Microsoft SQL Server 2017 o superior

### Configuración ###

* Modificar los valores del archivo de configuración "appsettings" correspondientes a:
* Web.Api
  * ConnectionString 
* Web.App
  * BaseURL
  * ConnectionString

Ejecutar el comando "update-migration" desde la consola de administracion de paquetes en el proyecto GNB.Infrastructure.
```